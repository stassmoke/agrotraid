$(document).ready(function () {

    let location = window.location.href;

    $('.list-inline li a').each(function () {
        let link = $(this).attr('href');

        if (location.indexOf(link) !== -1) {
            $(this).addClass('current');
        }
    });

    $('.menu-toggle').click(function () {

        $('ul').toggleClass('opening');
        $(this).toggleClass('open');
        $('.site-nav .shadow').toggleClass('shadow-enabled');

    });

    function startSlider() {
        $(".products-slider .activated .products-carousel .owl-carousel").owlCarousel({
            nav: true,
            dots: false,
            navText: ["", ""],
            margin: 20,
            // autoWidth: true,
            items: 3,
            responsive: {
                0: {
                    items: 1,
                    margin: 0,
                    // autoWidth: true,
                },
                500: {
                    items: 2
                },
                768: {
                    items: 3
                },
                891: {
                    items: 4
                },
                1199: {
                    items: 3
                },
                1268: {
                    items: 3
                },

                1920: {
                    items: 3
                },
            }
        });
    }

    startSlider();

    $("#products-mnu li").click(function (e) {
        let $this = $(this),
            item = $this.data('tab');

        e.preventDefault();

        $("#products-mnu li, .products-slider li").removeClass('activated');

        $this.addClass('activated');

        $(`#${item}`).addClass('activated');

        startSlider();
    });

    $("form").validate();

    let fancyTitle = $('.fancy_title');

    if (fancyTitle.length > 0) {
        fancyTitle.elipText({radius: 1500});
    }

});

function initMap() {

    new google.maps.Map(document.getElementById('map'), {
        center: {lat: 49.4225272, lng: 32.0999621},
        zoom: 16,
        styles: [
            {
                "featureType": "all",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#242f3e"
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#746855"
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#242f3e"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#e3c431"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#e3c431"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#263c3f"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#6b9a76"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#38414e"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#212a37"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#9ca5b3"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#746855"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#1f2835"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#f3d19c"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#2f3948"
                    }
                ]
            },
            {
                "featureType": "transit.station",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#d59563"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#17263c"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#515c6d"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#17263c"
                    }
                ]
            }
        ]
    });
}


